# PHP 7.3 is compatible with Drupal 8.6.4 and greater.
FROM php:7.3-apache-buster
# @see https://github.com/docker-library/php/blob/master/7.3/stretch/apache/Dockerfile

# PHP 7.0 can be used for projet using Drupal 7.0.8 to Drupal 8.7.0.
# FROM php:7.0-apache-stretch
# @see https://github.com/docker-library/php/blob/master/7.0/stretch/apache/Dockerfile

LABEL name=d8dev

# Get arguments set in docker-compose file.
ARG USER_UID
ARG USER_GID
ARG SSL_DOMAIN_NAME
ARG PATH_PROJECT
ARG PATH_DEFAULT

USER root

# Install the PHP extensions we need for Drupal 8.
# Note: Apache 7.3 dockerfile install standard PHP extensions: argon2, curl, FTP,
#       libedit, mbstring, mhash, mysqllnd, sodium, PDO sqlite, sqlite3,
#       openSSL, XML, Zlib.
RUN set -eux; \
	\
	if command -v a2enmod; then \
		a2enmod rewrite; \
	fi; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
	# Dev libraries required to build PHP extension GD.
		libfreetype6-dev \
		libjpeg-dev \
		libjpeg-turbo-progs \
    libjpeg62-turbo-dev \
		libpng-dev \
		# Dev libraries required to build PHP extension PDO PostgreSql.
#		libpq-dev \
		# Dev libraries required to build PHP extension Zip.
		libzip-dev \
		# Dev libraries to build PHP extension Intl.
		zlib1g-dev \
		libicu-dev \
	; \
	\
	docker-php-ext-configure gd \
		--with-freetype-dir=/usr \
		--with-jpeg-dir=/usr \
		--with-png-dir=/usr \
	; \
	docker-php-ext-configure intl; \
	docker-php-ext-install -j "$(nproc)" \
		gd \
		intl \
		opcache \
		pdo_mysql \
#		pdo_pgsql \
		zip

# Install PHP extension Xdebug.
RUN pecl install xdebug

# Install usefull software for developpers.
RUN apt-get install -y \
        default-mysql-client \
        git \
        imagemagick \
        pngquant \
        python \
        msmtp \
        sudo \
        unzip \
        wget \
        vim \
        nano \
        software-properties-common \
        bash-completion \
        rsyslog \
        iputils-ping \
        iproute2 \
        net-tools \
        msmtp \
        msmtp-mta

# Install PhpRedis.
RUN git clone https://github.com/phpredis/phpredis.git \
    && cd phpredis \
    && phpize \
    && ./configure \
    && make && make install \
    && cd .. \
    && rm -rf phpredis \
    && echo "extension=redis.so" > /usr/local/etc/php/conf.d/conf-redis.ini

# Enable SSL to support HTTPS:
# - Generate fake SSL certificate configuration,
# - Generate SSL certificate and key,
# - Update certificates list,
# - Enable Apache modules needed to manage HTTPS.
RUN openssl req -x509 -out /usr/local/share/ca-certificates/d8dev.crt \
    -keyout /usr/local/share/ca-certificates/d8dev.key \
    -newkey rsa:2048 -nodes -sha256 \
    -subj "/C=FR/ST=Paris/L=Paris/O=Drupal 8 developer/OU=None/CN=${SSL_DOMAIN_NAME}"; \
    update-ca-certificates; \
    a2enmod ssl headers

# Install NodeJS 12 LTS and Yarn.
RUN curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -; \
    apt-get update && apt-get install -y nodejs; \
    npm install yarn -g

# Install Uploadprogress
RUN git clone https://git.php.net/repository/pecl/php/uploadprogress.git \
    && cd uploadprogress \
    && phpize \
    && ./configure \
    && make && make install \
    && cd .. \
    && rm -rf uploadprogress \
    && echo "extension=uploadprogress.so" > /usr/local/etc/php/conf.d/conf-uploadprogress.ini

# Install Composer.
RUN cd /usr/local; \
    curl -sS https://getcomposer.org/installer | php; \
    chmod +x /usr/local/composer.phar; \
    ln -s /usr/local/composer.phar /usr/local/bin/composer; \
    # Install hirak/prestissimo.
    # This is a composer plugin that downloads packages in parallel to speed up the installation process.
    composer global require hirak/prestissimo

# Install yq, a portable command-line YAML processor.
#RUN APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CC86BB64; \
#    APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 add-apt-repository ppa:rmescandon/yq; \
#    apt-get update; \
#    apt-get install yq -y

# Install Symfony CLI.
#RUN  wget https://get.symfony.com/cli/installer -O - | bash; \
#     mv /root/.symfony/bin/symfony /usr/local/bin/symfony; \
#     rm -rf .symfony

# Install Phar Installation and Verification Environment (PHIVE).
RUN wget -O phive.phar https://phar.io/releases/phive.phar; \
#wget -O phive.phar.asc https://phar.io/releases/phive.phar.asc
#gpg --keyserver pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
#gpg --verify phive.phar.asc phive.phar
    chmod +x phive.phar; \
    mv phive.phar /usr/local/bin/phive

# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
#RUN	apt-mark auto '.*' > /dev/null; \
#	apt-mark manual $savedAptMark; \
#	ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
#		| awk '/=>/ { print $3 }' \
#		| sort -u \
#		| xargs -r dpkg-query -S \
#		| cut -d: -f1 \
#		| sort -u \
#		| xargs -rt apt-mark manual; \
#	\
#	apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false;

# Delete useless files to make Docker container light.
RUN apt-get clean && apt-get autoremove -q -y
RUN rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man /tmp/*

# Add user "dev" used by developer during his work.
# UID and GID have same number as default user in Debian like hosts.
RUN addgroup --gid $USER_UID "dev" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home "/home/dev" \
    --ingroup "dev" \
    --uid $USER_GID \
    "dev" \
    && adduser dev www-data \
    && adduser dev sudo \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN echo 'umask 002' >> /etc/bash.bashrc

# Create project/ directory with right owner and permissions.
# Note: all directories created by docker-compose have root:root owner.
RUN mkdir -p ${PATH_PROJECT}; \
  chown -R dev:www-data ${PATH_PROJECT}; \
  chmod -R 2775 ${PATH_PROJECT}

RUN mkdir -p /home/dev/.composer; \
  chown -R dev:dev /home/dev/.composer; \
  chmod -R 2775 /home/dev/.composer

# Nice prompt for Git.
RUN git clone https://github.com/magicmonty/bash-git-prompt.git /home/dev/.bash-git-prompt --depth=1; \
  chown -R dev:dev /home/dev/.bash-git-prompt

# Define working directory (automatically used after login to Docker).
WORKDIR ${PATH_DEFAULT}/

COPY docker-entrypoint /usr/local/bin/

RUN chmod a+x /usr/local/bin/docker-entrypoint

ENTRYPOINT ["docker-entrypoint"]

CMD ["apache2-foreground"]

