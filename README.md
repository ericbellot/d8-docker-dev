# Projet PHP EB

Installation d'un serveur Web LAMP basé sur Docker destiné aux développeurs Drupal ou Symfony.

## Orientations

Quelques principes orientent les choix effectués pour contruire ce projet :

* Rester le plus proche possible des images Docker officielles.
* Laisser au développeur la main sur les configurations du serveur :
  - le Dockerfile personnalisé contient le minimum de configuration possible,
  - les fichiers de configurations de PHP, d'Apache ou autres sont chargés par docker-compose. Il sont accessibles et éditables dans le répertoire `config/`.
  - La plupart des paramètres importants sont passés sous forme de variables d'environnement au conteneur et toutes modifiables dans le fichier `.env`.
* Fournir tous les outils standards nécessaires au développeur dans le Docker (Vim, Git, PhpMyAdmin, etc).
* *Ne pas fournir* Drupal. Il s'agit d'un autre projet: [d8-project]().
  
## Serveur LAMP

* Linux Debian 9 "Stretch" 
  
  Permet de gérer facilement les installations requises pour D7, D8 et D9.
* Apache 2.4.25
  Prêt pour D9.
* MariaDB 10.4
* PHP 7.3.16
  
  Possibilité de revenir à PHP 7.0 pour les premières version de Drupal 8 et les dernières de Drupal 7.
* PHPmyAdmin

  Pour accéder facilement à la base de données.
* Maildev

  Permet de récupérer et afficher les e-mails émis depuis le site Drupal.

## Outils disponibles dans le conteneur

* Bash, Bash Completion et Bash History
* cUrl
* Git
* Iproute2: pour avoir la commande ip.
* Nano
* Net tools: netstat, ifconfig, route, etc.
* NodeJS
* Ping
* Vim
* Unzip
* Wget

## Installation sur Ubuntu 18.04

### Pré-requis sur Ubuntu 18.04

Pour fonctionner Docker, Docker Compose et Git doivent être installés sur la machine hôte :

~~~
sudo apt install docker docker-compose git
~~~

### Installation de *D8-docker-dev* sur Ubuntu 18.04

1. Créer un répertoire pour le projet et se positionner dedans:

   ~~~
   mkdir ~/d8dockerdev
   cd ~/d8dockerdev
   ~~~
1. Cloner le dépôt

   ~~~
   git clone git@gitlab.com:ericbellot/d8-docker-dev.git .
   ~~~
1. Exécuter le script de configuration.

   ~~~
   bin/d8dd-configuration-ubuntu.bash
   ~~~
1. Éditer le fichier `.env` pour modifier la configuration:

   Les points important sont les suivants:
   
   - `DOMAIN_NAME`: définit le nom de domaine externe du site, celui qui sera utilisable depuis le navigateur de la machine hôte. S'il est modifié, il faut aussi modifier `SSL_DOMAIN_NAME` de façon correspondante, sinon l'HTTPS ne fonctionnera pas.
   - `USER_UID` et `USER_GID` définisse l'UID et le GID de l'utilisateur "dev" du conteneur *web*. ces deux ID doivent être identiques à celui de l'utilisateur de la machine hôte. Ainsi les fichiers seront éditables sont problème de permission depuis la ligne de commande du conteneur comme depuis la machine hôte.
   - les ports externes des différents services ne doivent pas entrer en conflit avec ceux de la machine hôte, les modifier en cas de besoins. Cela concerne les variables `WEB_HTTP_PORT`, `WEB_HTTPS_PORT`, `DB_PORT`, `MAILDEV_PORT` et `PHPMYADMIN_PORT`.
1. Démarrer les services avec Docker Compose:

   ~~~
   docker-compose up -d web
   ~~~
1. Connectez-vous en ligne de commande au service Web :

   ~~~
   docker-compose exec -u dev web bash
   ~~~
   
   On utilise l'utilisateur *dev* qui dispose de tous les droits permettant de travailler dans le site.
1. Créer un fichier PHP pour tester le fonctionnement du serveur:

   ~~~
   mkdir web
   touch web/index.php
   printf "<php\nphpinfo();" > web/index.php
   ~~~
   
   En vous connectant à l'adresse `https://localhost:8883`, les informations de configuration de PHP doivent s'afficher.
1. Pour pouvoir utiliser le nom de domaine, il faut l'ajouter au fichier `/etc/hosts` **de la machine hôte**:

  ~~~
  sudo echo "127.0.0.1 www.d8dev.localhost" >> /etc/hosts
  ~~~

### Tâches réalisées par `d8dd-configuration-ubuntu.bash`

Le script Bash `d8dd-configuration.bash` réalise les opérations suivantes:

1. Créer les répertoires permettant de conserver les données du site de façon persistente:

   - `data/db` pour le contenu de la base de données
   - `data.elasticsearch` pour les données du moteur de recherche ElasticSearch.
1. Copier le fichier `config/home/example_bash_history` pour créer le fichier de configuration `_bash_history` personnel :
       
   Les commandes Bash exécutées dans le service Web seront conservées d'une session de travail à l'autre.  
1. Copier le fichier `example.env` pour créer le fichier de configuration `.env` personnel :
   
1. Augmenter le max_map_count du système de fichier MMap utilisé par ElasticSearch pour stocker les index.

   Important: **ce paramètre doit être modifié sur la machine hôte**, car les index sont stockés dans le volume persistant.

   ~~~
   # Temporaire.
   sudo sysctl -w vm.max_map_count=262144
   # Permanent.
   sudo echo "vm.max_map_count=262144" >> /etc/sysctl.conf
   ~~~
   
   Plus d'information sur [ElasticSearch reference "Virual Memory"](https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html)

## Installation sur Windows 10 Professional

### Pré-requis sur Windows 10 Professional

Pour fonctionner Docker, Docker Compose et Git doivent être installés sur la machine hôte.

Télécharger et installer les logiciels suivants :

- [Git for Windows](https://gitforwindows.org/)
- [Docker Desktop CE (Community Edition)](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

L'installation de ces deux logiciel est tout à fait standard et ne pose pas de problème particulier.

### Installation de *D8-docker-dev* sur Windows 10 Professional

1. Ouvrir une console Git Bash depuis le menu Démarrer de Windows.

    Git Bash est une console Linux basé sur MinGW (*Minimalist GNU for Windows*), une distribution Linux légère destinée à être utlisée dans Windows.
    
    Git Bash est beaucoup plus confortable à utiliser que les console de Windows, et surtout permet d'utiliser Linux pour contrôler Docker.
1. Se placer dans la dossier du projets. 

    Par exemple, si on souhaite installer le projet dans `C:\projet\d8-docker-dev`, on fait :
    
    ~~~
    cd /c/projets
    mkdir d8-docker-dev
    cd d8-docker-dev
    ~~~
1. On clone le dépôt:

    ~~~
    # Ne pas oublier le point (dot) à la fin qui indique le répertoire où déployer le
    # dépôt. Ici "." indique le répertoire courant.
    git clone https://gitlab.com/ericbellot/d8-docker-dev.git .
    ~~~

1. Exécuter le script de configuration.

   ~~~
   bin/d8dd-configuration-windows.bash
   ~~~
1. Éditer le fichier `.env` pour modifier la configuration:

   Les points important sont les suivants:
   
   - `DOMAIN_NAME`: définit le nom de domaine externe du site, celui qui sera utilisable depuis le navigateur de la machine
      hôte. S'il est modifié, il faut aussi modifier `SSL_DOMAIN_NAME` de façon correspondante, sinon l'HTTPS ne
      fonctionnera pas.
   - `USER_UID` et `USER_GID` ne sont pas utilisées pour l'installtion Windows, les laissées telles quelles. 
   - les variables `WEB_HTTP_PORT`, `WEB_HTTPS_PORT`, `DB_PORT`, `MAILDEV_PORT` et `PHPMYADMIN_PORT` correspondent aux ports 
      extérieurs utilisés par Docker pour les contenuers. Attention à ce qu'il n'entrent pas en conflit avec des ports 
      déjà utilisés.
1. Démarrer le service Web avec Docker Compose:

   ~~~
   docker-compose up -d web
   ~~~
   
   Les conteneurs en dépendance seront automatiquement démarrés aussi.
1. Connectez-vous en ligne de commande au service Web :

   ~~~
    winpty docker-compose exec -u dev web bash
   ~~~
   
   On utilise l'utilisateur *dev* qui dispose de tous les droits permettant de travailler dans le site.
1. Créer un fichier PHP pour tester le fonctionnement du serveur:

   ~~~
   mkdir web
   touch web/index.php
   printf "<php\nphpinfo();" > web/index.php
   ~~~
   
   En vous connectant à l'adresse `https://localhost:8883`, les informations de configuration de PHP doivent s'afficher.
1. Pour pouvoir utiliser le nom de domaine, il faut l'ajouter au fichier `hosts` **de la machine hôte**:

    Windows n'autorise pas la modification directe du fichier :
    
     1. Copier le fichier `C:\Windows\System32\drivers\etc\hosts` sur votre bureau.
     1. Editez-le, et ajoutez-y la ligne ci-dessous:
     
          ~~~
          127.0.0.1 www.d8dev.localhost
          ~~~
        
        Remplacez `www.d8dev.localhost` par le nom de domaine que vous avez indiqué dans le paramètre
        `DOMAIN_NAME` du fichier `.env`.
     1. Recopiez-le dans le répertoire `C:\Windows\System32\drivers\etc\` afin d'écraser le fichier 
        `hosts` d'origine.
     1. Vous pouvez, à présent, accéder au site Web avec :
     
         + `http://www.d8dev.localhost`
         + ou `https://www.d8dev.localhost`

## Modification des paramètres d'un service existant

* Lorsqu'on modifie des paramètres dans le fichier `.env`, il faut redémarrer Docker Compose pour que les modifications soient prises en compte :

    ~~~
    docker-compose down
    docker-compose up -d --force-recreate
    ~~~

  Il faut effectuer un `docker-compose down` des services (un `docker-compose stop` ne suffit pas), et les relancer.

* Si les paramètres modifiés appartiennent à ceux utilisés durant la construction du conteneur *web*, il faut reconstruire le conteneur Docker:

    ~~~
    docker-compose build --no-cache web
    ~~~

  Paramètres utilisés durant la construction du conteneur *web* : 
    
    - `USER_UID`, 
    - `USER_GID`, 
    - `SSL_DOMAIN_NAME`,
    - `PATH_PROJECT`.

## Se connecter au conteneur Web de travail

Pour se connecter au conteneur `web` avec un hôte Linux:

~~~
docker-compose exec -u dev web bash
~~~

## Reconstruire le conteneur "web" 

Lorsqu'on modifie certains paramètres (UID de l'utilisateur, par exemple), il faut recontruire le conteneur Web.

~~~
docker-compose build --no-cache web
~~~

## Lorsqu'on rencontre un problème

~~~
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
~~~

Nettoyage des conteneurs inutilisés:

~~~
docker system prune --all
~~~