#!/bin/bash

mkdir -p data

# Create directory to store persistently database data.
mkdir -p data/db

# Create directory to store persistently ElasticSearch data.
mkdir -p data/elasticsearch

# Create project directory.
mkdir -p project

# Create file to store Bash history persistently.
cp -n config/home/example_bash_history config/home/_bash_history

# Create environment variables file. Should be curstomized.
cp -n example.env .env

# Create .gitmodules file
cp -n example.gitmodules .gitmodules
