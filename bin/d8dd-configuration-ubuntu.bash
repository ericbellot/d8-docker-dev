#!/bin/bash

if [ -f .env ]; then
  export $(grep ^USER_ .env  | xargs)
fi

mkdir -p data

# Create directory to store persistently database data.
mkdir -p data/db
sudo chown -R 999:$USER_GID data/db

# Create directory to store persistently ElasticSearch data.
mkdir -p data/elasticsearch
sudo chown -R 1001:$USER_GID data/elasticsearch

# Set count of MMap file system used by ElasticSearch.
# Note: this parameter is set on host machine because ElasticSearch data
# are stored in persistent volume data/elasticsearch in host machine.
# @see https://www.elastic.co/guide/en/elasticsearch/reference/current/vm-max-map-count.html
if [ "$(cat /proc/sys/vm/max_map_count)" -lt 262144 ]; then
  echo "vm.max_map_count=262144" | sudo tee -a /etc/sysctl.conf
  sudo sysctl -p
fi

# Create project directory.
mkdir -p project
sudo chown -R $USER_UID:33 project

# Create file to store Bash history persistently.
cp -p config/home/example_bash_history config/home/_bash_history

# Create environment variables file. Should be curstomized.
cp -p example.env .env

# Create .gitmodules file
cp -p example.gitmodules .gitmodules
